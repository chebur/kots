package ru.kots.presentation.vo;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemVo implements Parcelable {

    private String title;
    private String text;
    private String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.text);
        dest.writeString(this.url);
    }

    public ItemVo() {
    }

    protected ItemVo(Parcel in) {
        this.title = in.readString();
        this.text = in.readString();
        this.url = in.readString();
    }

    public static final Parcelable.Creator<ItemVo> CREATOR = new Parcelable.Creator<ItemVo>() {
        @Override
        public ItemVo createFromParcel(Parcel source) {
            return new ItemVo(source);
        }

        @Override
        public ItemVo[] newArray(int size) {
            return new ItemVo[size];
        }
    };
}
