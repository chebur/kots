package ru.kots.presentation.view.tabs;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.kots.R;
import ru.kots.presentation.presenter.ListPresenter;
import ru.kots.presentation.vo.ItemVo;

public class ListFragment extends Fragment implements ListAdapter.OnItemClickListener {

    private static final String KEY_LAYOUT_STATE = "layout_state";

    @BindView(R.id.rv_list)
    RecyclerView rvList;

    private OnFragmentInteractionListener listener;
    private Unbinder unbinder;
    private static final Map<String, ListFragment> instances = new HashMap<>();

    static ListFragment getInstance(@ListPresenter.Page String page) {
        if (!instances.containsKey(page)) {
            instances.put(page, new ListFragment());
        }
        return instances.get(page);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle state) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        if (state != null && state.containsKey(KEY_LAYOUT_STATE)) {
            Parcelable layoutState = state.getParcelable(KEY_LAYOUT_STATE);
            layoutManager.onRestoreInstanceState(layoutState);
        }
        rvList.setLayoutManager(layoutManager);
        rvList.setAdapter(new ListAdapter(this));
        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle state) {
        super.onSaveInstanceState(state);
        RecyclerView.LayoutManager layoutManager = rvList.getLayoutManager();
        if (layoutManager != null) {
            state.putParcelable(KEY_LAYOUT_STATE, layoutManager.onSaveInstanceState());
        }
    }

    void setData(@Nullable List<ItemVo> data) {
        if (rvList != null) {
            RecyclerView.Adapter adapter = rvList.getAdapter();
            if (adapter != null) {
                ((ListAdapter) adapter).setData(data);
            }
        }
    }

    @Override
    public void onItemClick(ItemVo item) {
        if (listener != null) {
            listener.onItemSelected(item);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
        listener = null;
    }

    public interface OnFragmentInteractionListener {
        void onItemSelected(ItemVo item);
    }

}
