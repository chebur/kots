package ru.kots.presentation.view.details;

import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.kots.R;
import ru.kots.presentation.vo.ItemVo;

public class DetailActivity extends AppCompatActivity {

    public static final String ARG_ITEM = "position";

    @BindView(R.id.iv_image)
    ImageView ivImage;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_text)
    TextView tvText;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_detail);
        unbinder = ButterKnife.bind(this);
        ItemVo item = getIntent().getParcelableExtra(ARG_ITEM);
        Picasso.get().load(Uri.parse(item.getUrl())).into(ivImage);
        tvTitle.setText(item.getTitle());
        tvText.setText(item.getText());
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
        super.onDestroy();
    }
}