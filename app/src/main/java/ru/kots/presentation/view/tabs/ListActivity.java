package ru.kots.presentation.view.tabs;

import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.tabs.TabLayout;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.kots.R;
import ru.kots.presentation.presenter.ListPresenter;
import ru.kots.presentation.presenter.ListPresenter.Page;
import ru.kots.presentation.view.details.DetailActivity;
import ru.kots.presentation.vo.ItemVo;

import static ru.kots.presentation.presenter.ListPresenter.Page.CAT;
import static ru.kots.presentation.presenter.ListPresenter.Page.DOG;

public class ListActivity extends AppCompatActivity implements ListFragment.OnFragmentInteractionListener {

    private static final String KEY_SELECTED_TAB = "selected_tab";

    private ListPresenter presenter;
    private ListFragment frTabCats;
    private ListFragment frTabDogs;
    private Unbinder unbinder;
    private @Page
    String selected = CAT;

    @BindView(R.id.cnt_pages)
    FrameLayout cntPages;

    @BindView(R.id.cnt_tabs)
    TabLayout cntTabs;

    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_tabs);
        unbinder = ButterKnife.bind(this);
        cntTabs.addOnTabSelectedListener(new OnTabSelectedListener());
        presenter = new ListPresenter(this);
        frTabCats = ListFragment.getInstance(CAT);
        frTabDogs = ListFragment.getInstance(DOG);
        if (state != null) {
            selected = state.getString(KEY_SELECTED_TAB, CAT);
        }
        TabLayout.Tab tab = cntTabs.getTabAt(CAT.equals(selected) ? 0 : 1);
        if (tab != null) {
            tab.select();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.attachView(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putString(KEY_SELECTED_TAB, selected);
    }

    @Override
    protected void onDestroy() {
        if (presenter != null) {
            presenter.detachView();
            presenter = null;
        }
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
        super.onDestroy();
    }

    public void updateTab() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        ListFragment fragment = CAT.equals(selected) ? frTabCats : frTabDogs;
        transaction.replace(R.id.cnt_pages, fragment);
        transaction.commit();
        manager.executePendingTransactions();
        Map<String, List<ItemVo>> data = presenter.getData();
        List<ItemVo> list = data.get(selected);
        if (list != null) {
            fragment.setData(list);
        }
    }

    public void onLoadingError(String type) {
        Toast.makeText(this, "Can't load page: " + type, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemSelected(ItemVo item) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.ARG_ITEM, item);
        startActivity(intent);
    }

    private class OnTabSelectedListener implements TabLayout.OnTabSelectedListener {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            selected = tab.getPosition() == 0 ? CAT : DOG;
            updateTab();
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            // do not need
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
            // do not need
        }
    }
}