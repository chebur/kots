package ru.kots.presentation.view.tabs;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.kots.R;
import ru.kots.presentation.vo.ItemVo;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private List<ItemVo> data = new LinkedList<>();
    private OnItemClickListener listener;

    ListAdapter(OnItemClickListener listener) {
        this.listener = listener;
    }

    public void setData(List<ItemVo> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemVo item = data.get(position);
        holder.tvTitle.setText(item.getTitle());
        holder.tvText.setText(item.getText());
        holder.loadImage(item.getUrl());
        ((View) holder.tvTitle.getParent()).setOnClickListener(v -> listener.onItemClick(item));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_image)
        ImageView ivImage;

        @BindView(R.id.tv_title)
        TextView tvTitle;

        @BindView(R.id.tv_text)
        TextView tvText;

        ViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        void loadImage(String url) {
            Picasso.get().load(Uri.parse(url)).into(ivImage);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ItemVo item);
    }
}