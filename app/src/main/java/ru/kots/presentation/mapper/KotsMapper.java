package ru.kots.presentation.mapper;

import java.util.LinkedList;
import java.util.List;

import ru.kots.domain.data.Response;
import ru.kots.domain.lo.ItemLo;
import ru.kots.presentation.vo.ItemVo;

public class KotsMapper {

    public List<ItemVo> mapData(Response response) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            sb.append(response.getMessage()).append(" ");
        }
        String text = sb.substring(0, sb.length() - 1);
        List<ItemVo> result = new LinkedList<>();
        for (ItemLo lo : response.getData()) {
            ItemVo vo = new ItemVo();
            vo.setText(text);
            vo.setTitle(lo.getTitle());
            vo.setUrl(lo.getUrl());
            result.add(vo);
        }
        return result;
    }
}