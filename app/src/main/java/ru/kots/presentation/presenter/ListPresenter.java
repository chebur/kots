package ru.kots.presentation.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.StringDef;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;
import ru.kots.domain.interactor.KotsInteractor;
import ru.kots.presentation.mapper.KotsMapper;
import ru.kots.presentation.view.tabs.ListActivity;
import ru.kots.presentation.vo.ItemVo;

import static android.content.Context.MODE_PRIVATE;
import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;
import static ru.kots.presentation.presenter.ListPresenter.Page.CAT;
import static ru.kots.presentation.presenter.ListPresenter.Page.DOG;

public class ListPresenter {

    private static final String PREFS_NAME = "prefs_name";
    private static final String KEY_DATA = "prefs_key_data";
    private ListActivity view;
    private KotsInteractor interactor;
    private KotsMapper mapper;
    private CompositeDisposable disposable = new CompositeDisposable();
    private Map<String, List<ItemVo>> data = new HashMap<>();

    public ListPresenter(Context context) {
        interactor = new KotsInteractor();
        mapper = new KotsMapper();
        loadCachedData(context);
    }

    public void attachView(ListActivity view) {
        this.view = view;
        if (data == null || data.size() == 0) {
            loadSource(CAT);
            loadSource(DOG);
        } else {
            view.updateTab();
        }
    }

    private void loadSource(@Page String type) {
        if (!isDataLoaded()) {
            disposable.add(
                    interactor
                            .get(type)
                            .observeOn(mainThread())
                            .map(response -> mapper.mapData(response))
                            .subscribe(
                                    response -> onDataLoaded(type, response),
                                    throwable -> onLoadingError(type, throwable))
            );
        }
    }

    public Map<String, List<ItemVo>> getData() {
        return data;
    }

    private void loadCachedData(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String strData = prefs.getString(KEY_DATA, null);
        if (strData != null) {
            Type type = new TypeToken<Map<String, List<ItemVo>>>() {
            }.getType();
            data = new Gson().fromJson(strData, type);
        }
    }

    private void onDataLoaded(@Page String type, List<ItemVo> items) {
        data.put(type, items);
        if (view != null && isDataLoaded()) {
            Context ctx = view.getApplicationContext();
            SharedPreferences prefs = ctx.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
            prefs.edit().putString(KEY_DATA, new Gson().toJson(data)).apply();
            view.updateTab();
        }
    }

    private boolean isDataLoaded() {
        return data != null && data.containsKey(CAT) && data.containsKey(DOG);
    }

    private void onLoadingError(@Page String type, Throwable throwable) {
        Log.e(getClass().getSimpleName(), "Error: ", throwable);
        if (view != null) {
            view.onLoadingError(type);
        }
    }

    public void detachView() {
        disposable.dispose();
        view = null;
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({CAT, DOG})
    public @interface Page {
        String CAT = "cat";
        String DOG = "dog";
    }
}