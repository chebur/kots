package ru.kots.domain.interactor;

import io.reactivex.Observable;
import ru.kots.domain.data.Response;
import ru.kots.domain.repo.KotsRepo;

public class KotsInteractor {

    private final KotsRepo repository;

    public KotsInteractor() {
        repository = new KotsRepo();
    }

    public Observable<Response> get(String query) {
        return repository.get(query);
    }

}