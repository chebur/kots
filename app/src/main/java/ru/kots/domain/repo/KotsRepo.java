package ru.kots.domain.repo;

import io.reactivex.Observable;
import ru.kots.domain.data.Api;
import ru.kots.domain.data.Response;

import static io.reactivex.schedulers.Schedulers.io;

public class KotsRepo {

    public Observable<Response> get(String query) {
        return Api.getInstance().getApi().get(query).subscribeOn(io());
    }

}