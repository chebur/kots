package ru.kots.domain.data;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class Api {

    private static final String ENDPOINT = "http://kot3.com/xim/";
    private static volatile Api instance;
    private KotsApi api;

    public static Api getInstance() {
        Api localInstance = instance;
        if (localInstance == null) {
            synchronized (Api.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Api();
                }
            }
        }
        return localInstance;
    }

    public KotsApi getApi() {
        if (api == null) {
            api = (new Retrofit.Builder())
                    .baseUrl(ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
                    .create(KotsApi.class);
        }
        return api;
    }

    public interface KotsApi {

        @GET("api.php")
        Observable<Response> get(@Query("query") String query);

    }
}