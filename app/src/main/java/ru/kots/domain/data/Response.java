package ru.kots.domain.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.kots.domain.lo.ItemLo;

public class Response {

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private List<ItemLo> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ItemLo> getData() {
        return data;
    }

    public void setData(List<ItemLo> data) {
        this.data = data;
    }
}